#include "Component.hpp"
#include <algorithm>    // std::max
#include <cmath>

namespace cgeditor {
class Scrollbar : public Component {
  /// @brief Set to true if it is the horizontal scrollbar
  bool IsHorizontal;
  Element bg,bar;
  double DragY,DragX;
  double FocusX,FocusY;
  /// @brief Should focus be applied on the next draw
  bool ShouldApplyFocus;
  /// @brief Use to detect scrollbar drags
  bool Trigger;
  /// @brief Goto a given graphical coordinate (if possible using current scroll range)
  void ApplyFocus();
public:
  Scrollbar(Status* s,bool IsHorizontal);
  void Refresh();
  /// @brief Focus on a coordinate on the next draw
  void Focus(double XorY);
  /// @brief Undo all scroll operations, go back to the origin
  void Reset();
};
} // namespace cgeditor